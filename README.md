# Pythonnet docker image
## Using image
1. Setup personal access token for your gitlab account https://gitlab.com/help/user/profile/personal_access_tokens.md
1. `docker login registry.gitlab.com` and paste your token when password were asked
1. Now you're free to do `docker pull {IMAG_NAME}'

FROM python:3.6.4-jessie
MAINTAINER Artem Zhukov <zhukov@remak.cz>

ENV PYTHONNET_VERSION master


RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF \
  && echo "deb http://download.mono-project.com/repo/debian jessie/snapshots/5.4.1.6/. main" > /etc/apt/sources.list.d/mono-official.list

RUN apt-get update
RUN apt-get install -y clang
RUN apt-get install -y mono-complete=5.4.1.6\*
RUN rm -fr /var/lib/apt/lists/* /tmp/*

RUN pip install pycparser

RUN git clone --branch "$PYTHONNET_VERSION" https://github.com/pythonnet/pythonnet
RUN python pythonnet/setup.py bdist_wheel
RUN pip install --no-index --find-links=./pythonnet/dist pythonnet
